@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10">
                <div class="card">
                    <div class="card-header">
                        Nákupní košík
                    </div>

                    <div class="card-body">
                        <div class="row">
                            @foreach ($products as $prod)

                            @php
                                $product = $prod['product'];
                                $count = $prod['count'];
                            @endphp

                                <div class="col-lg-3">
                                    <div class="card">
                                        <img src="{{ asset($product->image) }}" class="card-img-top" alt="{{ $product->name }}">

                                        <div class="card-body">
                                            <h2 class="card-title">
                                                <a href="/product/{{ $product->id }}">
                                                    {{ $product->name }}
                                                </a>
                                            </h2>

                                            <h3>{{ $product->price }} Kč</h3>

                                            <p class="card-text">{{ $product->description }}</p>
                                        </div>

                                        <div class="card-footer d-flex justify-content-end">

                                            <form action="/cart/remove/{{ $product->id }}" method="POST">
                                                @csrf

                                                <div class="input-group">
                                                    <input type="number" name="quantity" class="form-control text-right" value="{{ $count }}" min="0" style="max-width: 4rem">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">ks</span>
                                                        <button type="submit" class="btn btn-success">Upravit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="card">
                    <div class="card-header">
                        Zaplatit
                    </div>

                    <div class="card-body">
                        <p>Celková cena: {{ $totalPrice }} Kč</p>

                        <form action="/cart/checkout" method="post">
                            @csrf

                            <button type="submit" class="btn btn-success">Zaplatit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
