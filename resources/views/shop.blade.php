@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">

        <div class="col-lg-2">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    Kategorie
                </div>

                <div class="card-body p-0">
                    <ul class="list-group list-group-flush">
                        @foreach ($categories as $category)
                            @if(@request('category')->id == $category->id)
                                <li class="list-group-item d-flex justify-content-between active">
                                    <a href="/" class="text-light">{{ $category->name }}</a>
                                </li>
                            @else
                                <li class="list-group-item d-flex justify-content-between @if(@request('category')->id == $category->id) active @endif ">
                                    <a href="/category/{{ $category->id }}">{{ $category->name }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-10">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    Předměty
                </div>

                <div class="card-body">
                    <div class="row">
                        @foreach ($products as $product)
                            <div class="col-lg-3">
                                @include('products._detail')
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
