@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Nová kategorie</h1>

    <form action="/category" method="POST">
        @csrf

        @include('categories._form')

        <button type="submit" class="btn btn-primary">Uložit</button>
    </form>
</div>
@endsection
