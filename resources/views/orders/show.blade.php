@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Obědnávka č. {{ $order->id }}</h1>

    <h2>Info</h2>

    <ul class="list-group my-4">
        <li class="list-group-item">
            Datum: {{ $order->date_time }}
        </li>
        <li class="list-group-item">
            Celkvá cena: {{ $order->getTotalPrice() }} Kč
        </li>
    </ul>

    <h2>Zakoupené produkty</h2>

    <ul class="list-group my-4">
        @foreach ($order->products as $product)
            <li class="list-group-item">
                <a href="/product/{{ $product->id }}">
                    {{ $product->name }}, {{ $product->pivot->quantity }} ks, {{ $product->pivot->price }} Kč/ks
                </a>
            </li>
        @endforeach
    </ul>
</div>
@endsection
