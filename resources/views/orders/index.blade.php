@extends('layouts.app')

@section('content')
<div class="container">
    <ul class="list-group">
        @foreach ($orders as $order)
        <li class="list-group-item">
            <a href="/order/{{ $order->id }}">
                Obědnávka č. {{ $order->id }} - {{ $order->date_time }} - {{ $order->getTotalPrice() }} Kč
            </a>
        </li>
        @endforeach
    </ul>
</div>
@endsection
