@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Administrace kategorí</h1>

    <div class="row mb-4">
        <div class="list-group col-12">
            <a href="/category/create" class="list-group-item list-group-item-action">
                Nová kategorie
            </a>
        </div>
    </div>

    <h3>Kategorie</h3>

    <div class="row mb-4">
        <div class="list-group col-12">
            @foreach ($categories as $category)
                <div class="list-group-item d-flex justify-content-between">
                    <div>
                        {{ $category->name }}
                    </div>

                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="/category/edit/{{ $category->id }}" class="btn btn-primary btn-sm">Upravit</a>
                        <a href="/category/delete/{{ $category->id }}" class="btn btn-danger btn-sm">Odstranit</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

</div>
@endsection
