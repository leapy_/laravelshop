@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Administrace produktů</h1>

    <div class="row mb-4">
        <div class="list-group col-12">
            <a href="/product/create" class="list-group-item list-group-item-action">
                Nový produkt
            </a>
        </div>
    </div>

    <h3>Produkty</h3>

    <div class="row mb-4">
        <div class="list-group col-12">
            @foreach ($products as $product)
                <div class="list-group-item d-flex justify-content-between">
                    <div>
                        {{ $product->name }}
                    </div>

                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="/product/edit/{{ $product->id }}" class="btn btn-primary btn-sm">Upravit</a>
                        <a href="/product/delete/{{ $product->id }}" class="btn btn-danger btn-sm">Odstranit</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

</div>
@endsection
