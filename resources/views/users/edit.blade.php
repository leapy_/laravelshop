@extends('layouts.app')

@section('content')
<div class="container">
    <h1>{{ $user->name }}</h1>

    <form action="/user/edit/{{ $user->id }}" method="POST">
        @csrf

        @include('users._form')

        <button type="submit" class="btn btn-primary">Uložit</button>
    </form>
</div>
@endsection
