<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Jméno a příjmení</label>

    <div class="col-sm-10">
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', @$user->name) }}">

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email</label>

    <div class="col-sm-10">
        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', @$user->email) }}">

        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="phone" class="col-sm-2 col-form-label">Telefon</label>

    <div class="col-sm-10">
        <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone', @$user->phone) }}">

        @error('phone')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="city" class="col-sm-2 col-form-label">Město</label>

    <div class="col-sm-10">
        <input type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city', @$user->city) }}">

        @error('city')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="street" class="col-sm-2 col-form-label">Ulice</label>

    <div class="col-sm-10">
        <input type="text" class="form-control @error('street') is-invalid @enderror" name="street" value="{{ old('street', @$user->street) }}">

        @error('street')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="zip" class="col-sm-2 col-form-label">PSČ</label>

    <div class="col-sm-10">
        <input type="text" class="form-control @error('zip') is-invalid @enderror" name="zip" value="{{ old('zip', @$user->zip) }}">

        @error('zip')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
