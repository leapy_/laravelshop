@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Nový produkt</h1>

    <form action="/product" method="POST" enctype="multipart/form-data">
        @csrf

        @include('products._form')

        <button type="submit" class="btn btn-primary">Uložit</button>
    </form>
</div>
@endsection
