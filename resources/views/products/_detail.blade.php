<div class="card">
    <img src="{{ asset($product->image) }}" class="card-img-top" alt="{{ $product->name }}">

    <div class="card-body">
        <h2 class="card-title">
            <a href="/product/{{ $product->id }}">
                {{ $product->name }}
            </a>
        </h2>

        <h3>{{ $product->price }} Kč</h3>

        <p class="card-text">{{ $product->description }}</p>
    </div>

    <div class="card-footer d-flex justify-content-end">

        <form action="/cart/add/{{ $product->id }}" method="POST">
            @csrf

            <div class="input-group">
                <input type="number" name="quantity" class="form-control text-right" value="1" min="1" style="max-width: 4rem">
                <div class="input-group-append">
                    <span class="input-group-text">ks</span>
                    <button type="submit" class="btn btn-success">Do košíku</button>
                </div>
            </div>
        </form>
    </div>
</div>
