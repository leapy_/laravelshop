<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Název</label>

    <div class="col-sm-10">
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', @$product->name) }}">

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="description" class="col-sm-2 col-form-label">Popis</label>

    <div class="col-sm-10">
        <textarea class="form-control @error('description') is-invalid @enderror" name="description">{{ old('description', @$product->description) }}</textarea>

        @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-sm-2 col-form-label">Cena</label>

    <div class="col-sm-10">
        <input type="number" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price', @$product->price) }}">

        @error('price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="category_id" class="col-sm-2 col-form-label">Kategorie</label>

    <div class="col-sm-10">
        <select class="form-control @error('category_id') is-invalid @enderror" name="category_id">
            @foreach ($categories as $category)
                <option value="{{ $category->id }}" @if(old('category_id', @$product->category_id) == $category->id) selected @endif >{{ $category->name }}</option>
            @endforeach
        </select>

        @error('category_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="image" class="col-sm-2 col-form-label">Obrázek</label>

    <div class="col-sm-10">
        @if (@$product->image)
            <img src="{{ asset($product->image) }}" alt="{{ $product->name }}">
        @endif

        <input type="file" class="form-control-file @error('image') is-invalid @enderror" name="image">

        @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
