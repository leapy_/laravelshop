@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-lg-4 pb-4">
                <div class="card">
                    <img src="{{ asset($product->image) }}" class="card-img-top" alt="{{ $product->name }}">
                </div>
            </div>

            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">{{ $product->name }}</h2>
                        <p class="card-text">{{ $product->description }}</p>
                    </div>

                    <div class="card-footer d-flex justify-content-between">
                        <h3>{{ $product->price }} Kč</h3>

                        <form action="/cart/add/{{ $product->id }}" method="POST">
                            @csrf

                            <div class="input-group">
                                <input type="number" name="quantity" class="form-control text-right" value="1" min="1" style="max-width: 4rem">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-success">Do košíku</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
