@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Upravit produkt</h1>

    <form action="/product/edit/{{ $product->id }}" method="POST" enctype="multipart/form-data">
        @csrf

        @include('products._form')

        <button type="submit" class="btn btn-primary">Uložit</button>
    </form>
</div>
@endsection
