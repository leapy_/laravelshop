<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::post('/category', 'CategoryController@store');
Route::get('/category/create', 'CategoryController@create');
Route::get('/category/edit/{category}', 'CategoryController@edit');
Route::post('/category/edit/{category}', 'CategoryController@update');
Route::get('/category/delete/{category}', 'CategoryController@delete');
Route::get('/category/{category}', 'CategoryController@show');

Route::post('/product', 'ProductController@store');
Route::get('/product/create', 'ProductController@create');
Route::get('/product/edit/{product}', 'ProductController@edit');
Route::post('/product/edit/{product}', 'ProductController@update');
Route::get('/product/delete/{product}', 'ProductController@delete');
Route::get('/product/{product}', 'ProductController@show');

Route::get('/cart', 'CartController@index');
Route::post('/cart/add/{product}', 'CartController@add');
Route::post('/cart/remove/{product}', 'CartController@remove');
Route::post('/cart/checkout', 'CartController@checkout');

Route::get('/order', 'OrderController@index');
Route::get('/order/{order}', 'OrderController@show');

Route::get('/user/edit/{user}', 'UserController@edit');
Route::post('/user/edit/{user}', 'UserController@update');

Route::get('/admin/category', 'AdminController@category');
Route::get('/admin/product', 'AdminController@product');
