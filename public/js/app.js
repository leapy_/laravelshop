const numInputs = document.querySelectorAll('input[type=number]');

numInputs.forEach(function (input) {
    input.addEventListener('change', function (e) {
        if (e.target.value == '') {
            e.target.value = 1;
        }
    });
});
