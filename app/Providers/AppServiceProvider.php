<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Carbon;
use App\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Arr;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!session('cart')) {
            session(['cart' => []]);
        }

        Carbon::setToStringFormat('d.m.Y H:i');

        Gate::define('is-admin', function (User $user, ?User $model = null) {
            if ($user->id == 1) {
                return true;
            } else if ($model != null && $user->id == $model->id) {
                return true;
            } else {
                return false;
            }
        });

        View::composer('*', function ($view) {
            $view->with('cartCount', array_sum(session('cart')));
        });
    }
}
