<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(User $user)
    {
        $this->authorize('is-admin', $user);

        return view('users.edit', [
            'user' => $user,
        ]);
    }

    public function update(User $user)
    {
        $this->authorize('is-admin', $user);

        $user->update(request()->all());

        return redirect()->back()->with([
            'success' => 'Změny uloženy.',
        ]);
    }
}
