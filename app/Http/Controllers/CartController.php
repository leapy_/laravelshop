<?php

namespace App\Http\Controllers;

use App\Product;
use App\Order;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = [];
        $totalPrice = 0;

        foreach (session('cart') as $id => $count) {
            $product = Product::where('id', $id)->first();

            $products[] = [
                'product' => $product,
                'count' => $count,
            ];

            $totalPrice += $product->price * $count;
        }

        return view('cart', [
            'products' => $products,
            'totalPrice' => $totalPrice,
        ]);
    }

    public function add(Product $product)
    {
        request()->validate([
            'quantity' => ['required', 'integer', 'min:1'],
        ]);

        $count = session()->pull('cart.' . $product->id, 0) + request('quantity');
        if ($count > 0) session()->put('cart.' . $product->id, $count);

        return redirect()->back()->with([
            'success' => 'Předmět přidán do košíku.',
        ]);
    }

    public function remove(Product $product)
    {
        request()->validate([
            'quantity' => ['required', 'integer', 'min:0'],
        ]);

        if (request('quantity') <= 0) session()->pull('cart.' . $product->id);
        if (request('quantity') > 0) session()->put('cart.' . $product->id, request('quantity'));

        return redirect()->back()->with([
            'success' => 'Upraven počet položek v košíku.',
        ]);
    }

    public function checkout()
    {
        if (count(session('cart')) == 0) {
            return redirect()->back()->with([
                'error' => 'Nemužete zaplatit prázdný košík.',
            ]);
        }

        $order = Order::create([
            'user_id' => auth()->user()->id,
            'date_time' => now(),
        ]);

        foreach (session('cart') as $id => $count) {
            $product = Product::where('id', $id)->first();

            $order->products()->attach($product, [
                'price' => $product->price,
                'quantity' => $count,
            ]);
        }

        session()->pull('cart');

        return redirect('/')->with([
            'success' => 'Úspěšně zaplaceno.',
        ]);
    }
}
