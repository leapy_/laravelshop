<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Database\QueryException;

class CategoryController extends Controller
{
    public function show(Category $category)
    {
        return view('shop', [
            'categories' => Category::all(),
            'products' => $category->products,
        ]);
    }

    public function create()
    {
        $this->authorize('is-admin');

        return view('categories.create');
    }

    public function store()
    {
        $this->authorize('is-admin');

        request()->validate([
            'name' => ['required', 'string'],
        ]);

        Category::create(request()->all());

        return redirect('/');
    }

    public function edit(Category $category)
    {
        $this->authorize('is-admin');

        return view('categories.edit', [
            'category' => $category,
        ]);
    }

    public function update(Category $category)
    {
        $this->authorize('is-admin');

        $category->update(request()->all());

        return redirect()->back()->with([
            'success' => 'Kategorie upravena.',
        ]);
    }

    public function delete(Category $category)
    {
        $this->authorize('is-admin');

        try {
            $category->delete();
        } catch (QueryException $e) {
            return redirect()->back()->with([
                'error' => 'Kategorie se nepodařilo odstranit. Má přiřazené produkty.',
            ]);
        }

        return redirect()->back()->with([
            'success' => 'Kategorie odstraněna.',
        ]);
    }
}
