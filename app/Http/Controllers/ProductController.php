<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Database\QueryException;

class ProductController extends Controller
{

    public function show(Product $product)
    {
        return view('products.show', [
            'product' => $product,
        ]);
    }

    public function create()
    {
        $this->authorize('is-admin');

        return view('products.create', [
            'categories' => Category::all(),
        ]);
    }

    public function store()
    {
        $this->authorize('is-admin');

        request()->validate([
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'image' => ['required','image'],
            'price' => ['required', 'integer'],
            'category_id' => ['required', 'exists:categories,id'],
        ]);

        $product = Product::make(request()->all());

        if (request()->has('image')) {
            $product->image = request()->file('image')->store('images');
        }

        $product->save();

        return redirect('/');
    }

    public function edit(Product $product)
    {
        $this->authorize('is-admin');

        return view('products.edit', [
            'product' => $product,
            'categories' => Category::all(),
        ]);
    }

    public function update(Product $product)
    {
        $this->authorize('is-admin');

        if (request()->has('image')) {
            $product->image = request()->file('image')->store('images');
        }

        $product->update(request()->all());

        return redirect()->back()->with([
            'success' => 'Produkt upraven.',
        ]);
    }
    public function delete(Product $product)
    {
        $this->authorize('is-admin');

        try {
            $product->delete();
        } catch (QueryException $e) {
            return redirect()->back()->with([
                'error' => 'Produkt se nepodařilo odstranit. Má přiřazené objednávky.',
            ]);
        }

        return redirect()->back()->with([
            'success' => 'Produkt úspěšně odstraněn.',
        ]);
    }
}
