<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function category()
    {
        $this->authorize('is-admin');

        return view('admin.category', [
            'categories' => Category::all(),
        ]);
    }

    public function product()
    {
        $this->authorize('is-admin');

        return view('admin.product', [
            'products' => Product::all(),
        ]);
    }
}
