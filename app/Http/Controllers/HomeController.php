<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;

class HomeController extends Controller
{
    public function index()
    {
        return view('shop', [
            'categories' => Category::all(),
            'products' => Product::all(),
        ]);
    }
}
